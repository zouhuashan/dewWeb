var productModel = avalon.define({
    $id: 'productVMId',

    activeDishesIndex: 0,
    // 执行动画的楼层
    currentFloor: 0,

    // 通用顶部和底部HTML
    headerHtml: commHtml.topMenueHtml,
    footerHtml: commHtml.footerHtml,
    init: function () {
        setTimeout(function () {
            // 门店
            $(".product-door-item-text1").hover(function () {
                $(this).hide();
                $(this).siblings('.product-door-item-text2').show()
            })
            $(".product-door-item-text2").mouseleave(function () {
                $(".product-door-item-text2").hide();
                $(".product-door-item-text1").show()
            })

            $('.common-left-menu').stickUp();
        }, 200);
    },
    showDetail: function () {
        //示范一个公告层
        layer.open({
            type: 1,
            title: false, //不显示标题栏
            closeBtn: true,
            area: ['820px', '680px'],
            shade: 0.8,
            id: 'LAY_layuipro', //设定一个id，防止重复弹出
            btnAlign: 'c',
            shadeClose: true,
            scrollbar: false,
            moveType: 1, //拖拽模式，0或者1
            content: $('.product-detail'),
            success: function(layero){
            }
        });
    },
    changeActiveDishesIndex: function (index) {
        productModel.activeDishesIndex = index
    }
});

productModel.init();
