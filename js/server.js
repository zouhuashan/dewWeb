var serverModel = avalon.define({
    $id: 'serverVMId',

    floorHeightList: [690, 1196, 1657],

    activeDishesIndex: 0,
    // 执行动画的楼层
    currentFloor: 0,

    // 通用顶部和底部HTML
    headerHtml: commHtml.topMenueHtml,
    footerHtml: commHtml.footerHtml,
    init: function () {
        // 菜单
        setTimeout(function () {
            $('.common-left-menu').stickUp();

            // 执行一次锚点跳转
            serverModel.changeActiveDishesIndex(rUtil.parseUrl('type'));
        }, 200)
    },
    changeActiveDishesIndex: function (index) {
        // serverModel.activeDishesIndex = index
        if (index === 0) {
            $(window).scrollTop(0);
        } else {
            $(window).scrollTop(serverModel.floorHeightList[index - 1] - 480);
        }
    }
});

serverModel.init();

var heightList = [870, 1015, 1470, 1564, 1929, 2069];

$(window).scroll(function () {
    for (var i = 0; i < heightList.length; i++) {
        if ($(window).scrollTop() + $(window).height() > heightList[i]) {
            if (serverModel.currentFloor < (i + 1)) {
                serverModel.currentFloor = (i + 1);
            }
        }
    }

    if ($(window).scrollTop() + 480 < serverModel.floorHeightList[0]) {
        serverModel.activeDishesIndex = 0;
    } else if ($(window).scrollTop() + 480 >= serverModel.floorHeightList[0] && $(window).scrollTop() + 480 <= serverModel.floorHeightList[1]) {
        serverModel.activeDishesIndex = 1;
    } else if($(window).scrollTop() + 480 >= serverModel.floorHeightList[1] && $(window).scrollTop() + 480 <= serverModel.floorHeightList[2]) {
        serverModel.activeDishesIndex = 2;
    } else {
        serverModel.activeDishesIndex = 3;
    }

});
