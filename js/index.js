var homeModel = avalon.define({
    $id: 'homeVMId',

	activeDishesIndex: 0,
    // 执行动画的楼层
    currentFloor: 0,
    
    // 通用顶部和底部HTML
    headerHtml: commHtml.topMenueHtml,
	footerHtml: commHtml.footerHtml,
	init: function () {
    	// 顶部
        new Swiper ('#index-top-swiper-container', {
            loop: true,
            autoplay: true,
            grabCursor : true,
            roundLengths : true,
            autoHeight: true,

            // 如果需要分页器
            pagination: {
                el: '#index-top-swiper-pagination',
            }
        })

		// 新闻
        new Swiper ('#index-news-swiper-container', {
            autoplay: true,
            initialSlide: 0,
            slidesPerView : 4,
            spaceBetween : 60,
            grabCursor : true,

            // 如果需要前进后退按钮
            navigation: {
                nextEl: '#index-news-swiper-button-next',
                prevEl: '#index-news-swiper-button-prev',
            }
        })

		// 加盟鼠标动效
		$(".index-join-main-item").hover(function () {
            $(this).hide();
			$(this).siblings('.index-join-main-item-text').show()
        })
        $(".index-join-main-item-text").mouseleave(function () {
            $(".index-join-main-item-text").hide();
            $(".index-join-main-item").show()
        })
    },
	changeActiveDishesIndex: function (index) {
        homeModel.activeDishesIndex = index
	}
});

homeModel.init();

$(window).click(function (e) {
    console.log(e.pageY)
})

var heightList = [1000, 1584, 1780, 2293, 3151, 3458, 3963];
// 监听滚动条-取5个点
$(window).scroll(function () {
    for (var i = 0; i < heightList.length; i++) {
        if ($(window).scrollTop() + $(window).height() > heightList[i]) {
            if (homeModel.currentFloor < (i + 1)) {
                homeModel.currentFloor = (i + 1);
            }
        }
    }
});
