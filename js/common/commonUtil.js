
var rUtil = {
    projectName: 'dzw',

    // 获取url上面的参数
    parseUrl : function(key){
        var param = {};
        var url = document.URL;
        try{
            var _index1 = url.indexOf("?");
            if(_index1 > -1){
                var str = url.substring(_index1 + 1);
                var _index2 = str.indexOf("&");
                if(_index2 > -1){
                    //多个参数
                    var arr = str.split("&");
                    for(var i = 0, len = arr.length; i < len; i++){
                        var _index3 = arr[i].indexOf("=");
                        if(_index3 > -1){
                            var arr1_temp = arr[i].split("=");
                            param[arr1_temp[0]] = arr1_temp[1];
                        }
                    }
                }else{
                    //单个参数
                    var _index4 = str.indexOf("=");
                    if(_index4 > -1){
                        var arr2_temp = str.split("=");
                        param[arr2_temp[0]] = arr2_temp[1];
                    }
                }
            }
        }catch(e){
            throw "util.parseUrl解析异常";
        }
        if(!!key) return param[key];
        return param;
    }
}

/**
 * 接口通用js
 */
function dzw(){
    this.projectUrl = "http://122.114.61.111:8080";
}

/**
 * 获取数据
 * @param interfaceUrl 接口路径
 * @param params 参数
 * @param callback 回调方法
 * @param async 同步异步，可不传，默认异步
 */
dzw.prototype.getData = function(interfaceUrl,params,callback,async){
    if(async==undefined){
        async=true;
    }
    $.ajax({
        type: 'POST',
        url: this.projectUrl+interfaceUrl,
        data:params,
        dataType: "json",
        async: async,
        success: callback,
        error:function(data){
        }
    });
};


//加载依赖包
// jquery
document.write("<script language='javascript' charset='UTF-8' src='/" + rUtil.projectName + "/js/dist/jquery.min.js' ></script>");
// avalon
document.write("<script language='javascript' charset='UTF-8' src='/" + rUtil.projectName + "/js/dist/avalon.js' ></script>");

// 固定菜单
document.write("<script language='javascript' charset='UTF-8' src='/" + rUtil.projectName + "/js/dist/stickUp.js' ></script>");

// 通用HTML代码
document.write("<script language='javascript' charset='UTF-8' src='/" + rUtil.projectName + "/js/common/commonHtml.js' ></script>");
document.write("<script language='javascript' charset='UTF-8' src='/" + rUtil.projectName + "/js/common/commonAvalon.js' ></script>");

// 加载通用的css
document.write("<link rel='stylesheet' charset='UTF-8' href='/" + rUtil.projectName + "/css/common.css'></link>");
document.write("<link rel='stylesheet' charset='UTF-8' href='/" + rUtil.projectName + "/css/menu_style.css'></link>");

// 加载动画css
document.write("<link rel='stylesheet' charset='UTF-8' href='/" + rUtil.projectName + "/css/animate.css'></link>");