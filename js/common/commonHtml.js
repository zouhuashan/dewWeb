var commHtml = {

    // 通用顶部菜单
    topMenueHtml: [
        '<!-- 顶部菜单 -->' +
        '<div ms-controller="commonTopMenueVMId" class="top-menu-nav">' +
        '   <div class="common-work-space-top">' +
        '       <div class="top-menu-logo"></div>' +
        '       <nav>' +
        '           <ul class="content clearfix">' +
        '               <li><a href="javascript:;" ms-click="@goMenu(\'index\')" ms-class="[@activeIndex == 0 ? \'active-menu\' : \'\']">首页</a></li>' +
        '               <li class="dropdown">' +
        '                   <a href="javascript:;" ms-click="@goMenu(\'about\')" ms-class="[@activeIndex == 1 ? \'active-menu\' : \'\']">关于我们</a>' +
        '                   <ul class="sub-menu">' +
        '                       <li><a href="javascript:;" ms-click="@goMenu(\'about\', 0)">董事长致辞</a></li>' +
        '                       <li><a href="javascript:;" ms-click="@goMenu(\'about\', 1)">企业介绍</a></li>' +
        '                       <li><a href="javascript:;" ms-click="@goMenu(\'about\', 2)">发展历程</a></li>' +
        '                       <li><a href="javascript:;" ms-click="@goMenu(\'about\', 3)">企业荣誉</a></li>' +
        '                   </ul>' +
        '               </li>' +
        '               <li class="dropdown">' +
        '                   <a href="javascript:;" ms-click="@goMenu(\'chain\')" ms-class="[@activeIndex == 2 ? \'active-menu\' : \'\']">连锁加盟</a>' +
        '                   <ul class="sub-menu">' +
        '                       <li><a href="javascript:;" ms-click="@goMenu(\'chain\', 0)">全国连锁</a></li>' +
        '                       <li><a href="javascript:;" ms-click="@goMenu(\'chain\', 1)">加盟店风采</a></li>' +
        '                       <li><a href="javascript:;" ms-click="@goMenu(\'chain\', 2)">招商加盟</a></li>' +
        '                       <li><a href="javascript:;" ms-click="@goMenu(\'chain\', 2)">加盟流程</a></li>' +
        '                       <li><a href="javascript:;" ms-click="@goMenu(\'chain\', 2)">表单下载</a></li>' +
        '                   </ul>' +
        '               </li>' +
        '               <li class="dropdown">' +
        '                   <a href="javascript:;" ms-click="@goMenu(\'product\')" ms-class="[@activeIndex == 3 ? \'active-menu\' : \'\']">产品中心</a>' +
        '                   <ul class="sub-menu">' +
        '                       <li><a href="javascript:;" ms-click="@goMenu(\'product\', 0)">牛油系列</a></li>' +
        '                       <li><a href="javascript:;" ms-click="@goMenu(\'product\', 1)">清油系列</a></li>' +
        '                       <li><a href="javascript:;" ms-click="@goMenu(\'product\', 2)">山珍系列</a></li>' +
        '                       <li><a href="javascript:;" ms-click="@goMenu(\'product\', 3)">自热方便火锅</a></li>' +
        '                   </ul>' +
        '               </li>' +
        '               <li class="dropdown">' +
        '                   <a href="javascript:;" ms-click="@goMenu(\'dishes\')" ms-class="[@activeIndex == 4 ? \'active-menu\' : \'\']">菜品展示</a>' +
        '                   <ul class="sub-menu">' +
        '                       <li><a href="javascript:;" ms-click="@goMenu(\'dishes\', 0)">特色菜品</a></li>' +
        '                       <li><a href="javascript:;" ms-click="@goMenu(\'dishes\', 1)">荤菜系列</a></li>' +
        '                       <li><a href="javascript:;" ms-click="@goMenu(\'dishes\', 2)">素菜系列</a></li>' +
        '                   </ul>' +
        '               </li>' +
        '               <li class="dropdown">' +
        '                   <a href="javascript:;" ms-click="@goMenu(\'news\')" ms-class="[@activeIndex == 5 ? \'active-menu\' : \'\']">新闻中心</a>' +
        '                   <ul class="sub-menu">' +
        '                       <li><a href="javascript:;" ms-click="@goMenu(\'news\', 0)">公司动态</a></li>' +
        '                       <li><a href="javascript:;" ms-click="@goMenu(\'news\', 1)">行业动态</a></li>' +
        '                       <li><a href="javascript:;" ms-click="@goMenu(\'news\', 2)">公告公示</a></li>' +
        '                   </ul>' +
        '               </li>' +
        '               <li class="dropdown">' +
        '                   <a href="javascript:;" ms-click="@goMenu(\'server\')" ms-class="[@activeIndex == 6 ? \'active-menu\' : \'\']">客服中心</a>' +
        '                   <ul class="sub-menu">' +
        '                       <li><a href="javascript:;" ms-click="@goMenu(\'server\', 0)">联系我们</a></li>' +
        '                       <li><a href="javascript:;" ms-click="@goMenu(\'server\', 1)">在线订购</a></li>' +
        '                       <li><a href="javascript:;" ms-click="@goMenu(\'server\', 2)">表单下载</a></li>' +
        '                       <li><a href="javascript:;" ms-click="@goMenu(\'server\', 3)">在线留言</a></li>' +
        '                   </ul>' +
        '               </li>' +
        '               <li><a href="javascript:;">火锅商城</a></li>' +
        '           </ul>' +
        '       </nav>' +
        '   </div>' +
        '</div>'
    ],

    // 通用底部HTML代码
    footerHtml: [
        '<!--通用底部-->' +
        '<div class="footer">' +
        '   <div class="common-work-space">' +
        '       <div class="footer-logo"></div>' +
    '           <div class="footer-text">' +
        '           <p>加盟电话 : 400-615-1331</p>' +
        '           <p>销售电话 : 400-999-7387</p>' +
        '           <p>四川火锅商城网址 : http://www.dieziwei.cn</p>' +
        '           <p>CopyRight &copy; 版权所有 : 四川碟滋味餐饮有限公司 网站地图 XML</p>' +
        '       </div>' +
        '       <div class="footer-img">' +
        '           <img src="img/ercode.jpg" height="93" width="93"/>' +
        '       </div>' +
        '   </div>' +
        '</div>'
    ],
}