var newsModel = avalon.define({
    $id: 'newsVMId',

    activeDishesIndex: 0,
    // 执行动画的楼层
    currentFloor: 0,

    // 通用顶部和底部HTML
    headerHtml: commHtml.topMenueHtml,
    footerHtml: commHtml.footerHtml,
    init: function () {
        setTimeout(function () {
            $('.common-left-menu').stickUp();
        }, 200);
    },
    changeActiveDishesIndex: function (index) {
        newsModel.activeDishesIndex = index
    }
});

newsModel.init();
