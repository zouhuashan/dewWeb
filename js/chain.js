var chainModel = avalon.define({
    $id: 'chainVMId',

    floorHeightList: [1030, 1676],

	activeDishesIndex: 0,
    // 执行动画的楼层
    currentFloor: 0,
    
    // 通用顶部和底部HTML
    headerHtml: commHtml.topMenueHtml,
	footerHtml: commHtml.footerHtml,
	init: function () {
        setTimeout(function () {
            // 门店
            $(".chain-door-item-text1").hover(function () {
                $(this).hide();
                $(this).siblings('.chain-door-item-text2').show()
            })
            $(".chain-door-item-text2").mouseleave(function () {
                $(".chain-door-item-text2").hide();
                $(".chain-door-item-text1").show()
            })

            $('.common-left-menu').stickUp();

            // 执行一次锚点跳转
            chainModel.changeActiveDishesIndex(rUtil.parseUrl('type'));
        }, 200);
    },
	changeActiveDishesIndex: function (index) {
        chainModel.activeDishesIndex = index
        if (index === 0) {
            $(window).scrollTop(0);
        } else {
            $(window).scrollTop(chainModel.floorHeightList[index - 1] - 480);
        }
	}
});

chainModel.init();
//
// $(window).click(function (e) {
//     console.log(e.pageY)
// })

var heightList = [1053, 1178, 1739, 1831];

$(window).scroll(function () {

    for (var i = 0; i < heightList.length; i++) {
        if ($(window).scrollTop() + $(window).height() > heightList[i]) {
            if (chainModel.currentFloor < (i + 1)) {
                chainModel.currentFloor = (i + 1);
            }
        }
    }

    if ($(window).scrollTop() + 480 >= chainModel.floorHeightList[1]) {
        chainModel.activeDishesIndex = 2;
    } else if ($(window).scrollTop() + 480 < chainModel.floorHeightList[0]) {
        chainModel.activeDishesIndex = 0;
    } else {
        chainModel.activeDishesIndex = 1;
    }
});
