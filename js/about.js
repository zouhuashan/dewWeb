var aboutModel = avalon.define({
    $id: 'aboutVMId',

    floorHeightList: [922, 1833, 2457],

    activeDishesIndex: 0,
    // 执行动画的楼层
    currentFloor: 0,

    // 通用顶部和底部HTML
    headerHtml: commHtml.topMenueHtml,
    footerHtml: commHtml.footerHtml,
    init: function () {
        // 菜单
        setTimeout(function () {
            $('.common-left-menu').stickUp();

            // 执行一次锚点跳转
            aboutModel.changeActiveDishesIndex(rUtil.parseUrl('type'));
        }, 200)
    },
    changeActiveDishesIndex: function (index) {
        // aboutModel.activeDishesIndex = index
        if (index === 0) {
            $(window).scrollTop(0);
        } else {
            $(window).scrollTop(aboutModel.floorHeightList[index - 1] - 480);
        }
    }
});

aboutModel.init();

$(window).click(function (e) {
    console.log(e.pageY)
})

var heightList = [948, 955, 1774, 1872, 2412, 2512];

$(window).scroll(function () {
    for (var i = 0; i < heightList.length; i++) {
        if ($(window).scrollTop() + $(window).height() > heightList[i]) {
            if (aboutModel.currentFloor < (i + 1)) {
                aboutModel.currentFloor = (i + 1);
            }
        }
    }

    if ($(window).scrollTop() + 480 < aboutModel.floorHeightList[0]) {
        aboutModel.activeDishesIndex = 0;
    } else if ($(window).scrollTop() + 480 >= aboutModel.floorHeightList[0] && $(window).scrollTop() + 480 <= aboutModel.floorHeightList[1]) {
        aboutModel.activeDishesIndex = 1;
    } else if($(window).scrollTop() + 480 >= aboutModel.floorHeightList[1] && $(window).scrollTop() + 480 <= aboutModel.floorHeightList[2]) {
        aboutModel.activeDishesIndex = 2;
    } else {
        aboutModel.activeDishesIndex = 3;
    }

});
