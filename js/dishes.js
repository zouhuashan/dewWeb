var vm = avalon.define({
    $id: 'body',
    dishesList: []
})

var dzw = new dzw();

getTypeList(1);

function getTypeList(pageNo){
    dzw.getData("/api/getProduct",{
        pageSize:12,
        pageNo:pageNo,
        params:{
            "productType":1
        }
    },function(data){
        console.log(data.result)
        vm.dishesList = data.result;
        if(pageNo==1){
            layui.use(['laypage', 'layer'], function(){
                var laypage = layui.laypage,
                    layer = layui.layer;
                laypage.render({
                    elem: 'pages',
                    pages: data.totalRecords,
                    limit: 12,
                    theme: '#ed4132',
                    jump: function(obj, first){
                        console.log(obj)
                        if(!first){
                            getTypeList(obj.curr);
                        }
                    }
                });
            });
        }
    });
}

dzw.getData("/industry/v1/load_industrys.json",{},function(data){
    vm.types = data.data.resList;
    vm.priceList = data.data.priceList;
},false);